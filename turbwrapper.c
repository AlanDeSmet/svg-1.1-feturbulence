#include "turb.h"

static double turbulence_no_stitch(int nColorChannel, double *point, double fBaseFreqX, double fBaseFreqY,
			  int nNumOctaves, bool bFractalSum) {
	bool bDoStitching = 0;
	// These are all only used when bDoStitching==True, so their value doesn't matter
	double fTileX = 0.0;
	double fTileY = 0.0;
	double fTileWidth = 0.0;
	double fTileHeight = 0.0;
	return turbulence(nColorChannel, point, fBaseFreqX, fBaseFreqY,
			  nNumOctaves, bFractalSum, bDoStitching,
			  fTileX, fTileY, fTileWidth, fTileHeight);
}

static double clamp_to_0_1(double f) {
	if(f>1.0) { return 1.0; }
	if(f<0.0) { return 0.0; }
	return f;
}

double type_fractalNoise(double *point, double fBaseFreq, int nNumOctaves) {

	/* SVG 2 Filter Effects: 
	 * "For fractalSum, you get a turbFunctionResult that is aimed at a range of
	 * -1 to 1 (the actual result might exceed this range in some cases). To
	 * convert to a color or alpha value, use the formula 
	 *     colorValue = (turbFunctionResult + 1) / 2
	 * , then clamp to the range 0 to 1."
	 */

	int nColorChannel = 0;
	bool bFractalSum = 1;
	double turbFunctionResult = turbulence_no_stitch(nColorChannel, point, fBaseFreq, fBaseFreq, nNumOctaves, bFractalSum);
	double colorValue = (turbFunctionResult+1.0)/2.0;
	return clamp_to_0_1(colorValue);
}

double type_turbulence(double *point, double fBaseFreq, int nNumOctaves) {
	/* SVG 2 Filter Effects: 
	 *
	 * "For turbulence, you get a turbFunctionResult that is aimed at a range of
	 * 0 to 1 (the actual result might exceed this range in some cases). To
	 * convert to a color or alpha value, use the formula 
	 *     colorValue = turbFunctionResult
	 * , then clamp to the range 0 to 1."
	 */

	int nColorChannel = 0;
	bool bFractalSum = 0;
	double turbFunctionResult = turbulence_no_stitch(nColorChannel, point, fBaseFreq, fBaseFreq, nNumOctaves, bFractalSum);
	double colorValue = turbFunctionResult;
	return clamp_to_0_1(colorValue);
}


