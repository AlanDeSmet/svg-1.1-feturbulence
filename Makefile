TARGET=noise
OBJS=main.o turb.o turbwrapper.o
CFLAGS=-g -Wall -Werror
LDFLAGS=-lm

$(TARGET): $(OBJS)
	gcc $^ $(LDFLAGS) -o $@

clean:
	rm -f $(TARGET) $(OBJS) fractalNoise.png turbulence.png
