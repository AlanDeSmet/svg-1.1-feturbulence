#ifndef TURB_C
#define TURB_C

/* Turbulence implementation from SVG 2 standard */
void init_turbulence(long lSeed);

typedef int bool;

double turbulence(int nColorChannel, double *point, double fBaseFreqX, double fBaseFreqY,
          int nNumOctaves, bool bFractalSum, bool bDoStitching,
          double fTileX, double fTileY, double fTileWidth, double fTileHeight);

/* Wrappers implementing the details of fractalNoise vs turbulence
 *
 * (and simplifying to options I'm interested in)
 */

double type_fractalNoise(double *point, double fBaseFreq, int nNumOctaves);
double type_turbulence(double *point, double fBaseFreq, int nNumOctaves);

#endif
