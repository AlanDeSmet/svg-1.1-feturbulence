This is a small project to generate Perlin noise and turbulence based on the example code in the SVG 1.1 standard, as found at https://www.w3.org/TR/SVG11/filters.html#feTurbulenceElement  . I've made minimal changes to that code (found in turb.c) to allow it to compile (it's not valid C nor C++ as published). Everything else is just code to write some PNG output files.

Like the original code, in _theory_ the gradient generation code might create several 0 length vectors, which would cause a divide-by-zero. However, it includes an implementation of `random()` that may not do so in practice; I literally don't know. In production code you should loop until `s` in `init_turbulence` is non-zero; so something like this (_untested_):

```
      s = 0;
      while(s == 0) {
        for (j = 0; j < 2; j++)
          fGradient[k][i][j] = (double)(((lSeed = random(lSeed)) % (BSize + BSize)) - BSize) / BSize;
        s = (double)(sqrt(fGradient[k][i][0] * fGradient[k][i][0] + fGradient[k][i][1] * fGradient[k][i][1]));
      }
```

This little project is in service of my trying to figure out why my SVG filters ended up with thin dark lines in places I didn't expect. If you're curious, here's why:
https://stackoverflow.com/questions/68707019/why-are-there-thin-dark-lines-in-my-svg-feturbulence-output/68746951

`turb.c` is from the SVG 1.1 filter specification; I don't know who claims the copyright.

`stb_image_write.h` is from stb's excellent [Single File Public Domain Libraries](https://github.com/nothings/stb). It's in the public domain.

Everything else is (Alan De Smet's) work, and I release it into the public domain. (Indeed, the `Makefile` and `turb.h` are probably not original enough to be worthy of copyright, but `main.c` and `turbwrapper.c` _might_ be.)
