#include "turb.h"

#define STB_IMAGE_WRITE_IMPLEMENTATION
#include "stb_image_write.h"

#include <stdio.h>



int main(int argc, char *argv[]) {
	init_turbulence(0);
	float XMIN = 0.0;
	float XMAX = 10.0;
	float YMIN = 0.0;
	float YMAX = 10.0;
	float base_frequency = 1.0;
	const int COLUMNS = 300;
	const int ROWS = 300;
	double stepX = (XMAX-XMIN) / (float)(COLUMNS);
	double stepY = (YMAX-YMIN) / (float)(ROWS);
	float min_found_turb = 999;
	float max_found_turb = -999;
	float min_found_frac = 999;
	float max_found_frac = -999;
	int octaves = 1;

	printf("stepX: %f\n", stepX);
	printf("stepY: %f\n", stepY);

	char resultsTurb[ROWS*COLUMNS];
	char resultsFrac[ROWS*COLUMNS];

	for(int outy = 0; outy < ROWS; outy++) {
		for(int outx = 0; outx < COLUMNS; outx++) {
			int idx = outx+outy*COLUMNS;
			//printf("%d,%d -> %d\n", outx, outy, idx);
			double inx = stepX*outx;
			double iny = stepY*outy;
			double point[2] = {inx,iny};

			float resultTurb = type_turbulence(point, base_frequency, octaves);
			resultsTurb[idx] = (char)(resultTurb*256);

			float resultFrac = type_fractalNoise(point, base_frequency, octaves);
			resultsFrac[idx] = (char)(resultFrac*256);

			if( resultTurb> max_found_turb) { max_found_turb = resultTurb; }
			if( resultTurb< min_found_turb) { min_found_turb = resultTurb; }
			if( resultFrac> max_found_frac) { max_found_frac = resultFrac; }
			if( resultFrac< min_found_frac) { min_found_frac = resultFrac; }
		}
	}

	printf("turbulence   range is %f  to   %f\n",min_found_turb,max_found_turb);
	printf("fractalNoise range is %f  to   %f\n",min_found_frac,max_found_frac);
	int channels = 1;
	stbi_write_png("turbulence.png", COLUMNS, ROWS, channels, resultsTurb, COLUMNS);
	stbi_write_png("fractalNoise.png", COLUMNS, ROWS, channels, resultsFrac, COLUMNS);
}
